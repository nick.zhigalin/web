import { Theme } from '@material-ui/core';

export const styles = (theme: Theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
});
