import { WithStyles } from '@material-ui/core';
import { IRouterProps } from '../../lib/types';
import { styles } from './styles';

export interface IProps extends WithStyles<typeof styles>, IRouterProps {

}

export interface IState {

}
