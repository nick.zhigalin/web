import { createStyles } from '@material-ui/core/styles';

export const styles = () => createStyles<string, any>({
  paper: {
    height: '100%',
  },
});
