export default {
  chartData: [
    { month: 'Январь', sale: 50, total: 987 },
    { month: 'Февраль', sale: 100, total: 3000 },
    { month: 'Март', sale: 30, total: 1100 },
    { month: 'Апрель', sale: 107, total: 7100 },
    { month: 'Май', sale: 95, total: 4300 },
    { month: 'Июнь', sale: 150, total: 7500 },
  ],
};
