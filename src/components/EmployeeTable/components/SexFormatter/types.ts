export interface ISexLabels {
  /** Ярлык для мужского пола */
  male: 'Муж.';
  /** Ярлык для женского пола */
  female: 'Жен.';

  [index: string]: 'Муж.' | 'Жен.';
}
