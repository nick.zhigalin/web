import { createStyles, Theme } from '@material-ui/core';

export const styles = (theme: Theme) => createStyles<string, any>({
  textField: {
    marginBottom: theme.spacing(2),
  },
});
