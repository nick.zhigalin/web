import { createStyles } from '@material-ui/core';

export const styles = () => createStyles<string, any>({
  numericInput: {
    width: '100%',
  },
});
